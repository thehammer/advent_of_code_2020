defmodule AdventOfCode do
  @base_url "https://adventofcode.com/2020/"
  @cookie_file "cookie"

  def get_input(day) do
    HTTPoison.get!(@base_url <> "day/#{day}/input", %{}, hackney: [cookie: [cookies()]]).body
    |> String.trim_trailing
  end

  def cookies do
    if File.exists?(@cookie_file) do
      File.read!(@cookie_file)
      |> String.split("\n")
      |> Enum.join("; ")
    else
      ""
    end
  end
end
