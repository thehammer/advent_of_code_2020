defmodule AdventOfCode2020.MixProject do
  use Mix.Project

  def project do
    [
      app: :advent_of_code_2020,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bunt, "~> 0.2.0"},
      {:deque, "~> 1.0"},
      {:flow, "~> 0.12.0"},
      {:httpoison, "~> 1.7"},
      {:httpotion, "~> 3.0"},
    ]
  end
end
