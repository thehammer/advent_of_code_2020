defmodule Script do
  @sum 2020

  def main(_args) do
    AdventOfCode.get_input(1)
    |> String.split("\n")
    |> Stream.map(&String.to_integer/1)
    |> Enum.reduce_while(MapSet.new(), fn i, seen ->
      complement = @sum - i
      sub_product = complement_product(complement, i, seen)
      if is_integer(sub_product) do
        {:halt, i * sub_product}
      else
        {:cont, seen |> MapSet.put(i)}
      end
    end)
    |> IO.puts
  end

  def complement_product(sum, value, others) do
   others
   |> Enum.reduce_while(MapSet.new(), fn i, seen ->
      complement = sum - i
      if seen |> MapSet.member?(complement) do
        {:halt, i * complement}
      else
        {:cont, seen |> MapSet.put(i)}
      end
    end)
  end
end

Script.main(System.argv)
