defmodule Script do
  def main(_args) do
    AdventOfCode.get_input(2)
    |> String.split("\n")
    |> Stream.map(&parse/1)
    |> Stream.filter(&valid?/1)
    |> Enum.count
    |> IO.puts
  end

  defp parse(line) do
    [range, character, password] = line |> String.split(" ")
    [min, max] = range |> String.split("-") |> Enum.map(&String.to_integer/1)
    character = character |> String.trim_trailing(":")

    %{min: min, max: max, character: character, password: password}
  end

  defp valid?(%{min: min, max: max, character: character, password: password}) do
    count = password |> String.graphemes |> Stream.filter(& &1 == character) |> Enum.count

    count >= min && count <= max
  end
end

Script.main(System.argv)
