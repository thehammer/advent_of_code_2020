defmodule Script do
  def main(_args) do
    AdventOfCode.get_input(2)
    |> String.split("\n")
    |> Stream.map(&parse/1)
    |> Stream.filter(&valid?/1)
    |> Enum.count
    |> IO.puts
  end

  defp parse(line) do
    [positions, character, password] = line |> String.split(" ")
    [first, second] = positions |> String.split("-") |> Enum.map(&String.to_integer/1)
    character = character |> String.trim_trailing(":")

    %{first: first, second: second, character: character, password: password}
  end

  defp valid?(%{first: first, second: second, character: character, password: password}) do
    a = password |> String.at(first - 1)
    b = password |> String.at(second - 1)

    [a, b] |> Stream.filter(& &1 == character) |> Enum.count |> Kernel.==(1)
  end
end

Script.main(System.argv)
