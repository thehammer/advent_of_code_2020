defmodule Script do
  def main(_args) do
    map = AdventOfCode.get_input(3)
    |> String.split("\n")
    |> Enum.map(&parse/1)

    new = map
    |> traverse(3, 1)

    # [map, new]
    # |> Enum.zip
    # |> Enum.each(fn {a, b} -> IO.puts "#{a} | #{b}" end)

    new
    |> Stream.flat_map(& &1)
    |> Stream.filter(& &1 == "X")
    |> Enum.count
    |> IO.puts
  end

  defp parse(line) do
    line |> String.graphemes
  end

  defp traverse(map, right, down) do
    map
    |> Stream.with_index
    |> Enum.map(fn
      {row, index} when index > 0 and rem(index, down) == 0 ->
        x_offset = offset(right, down, index, Enum.count(row))
        marker = if Enum.at(row, x_offset) == "#", do: "X", else: "O"
        row |> List.replace_at(x_offset, marker)
      {row, _index} -> row
    end)
  end

  defp offset(right, down, index, length), do: rem(div(index, down) * right, length)
end

Script.main(System.argv)
