defmodule Script do
  def main(_args) do
    map = AdventOfCode.get_input(3)
    |> String.split("\n")
    |> Enum.map(&String.graphemes/1)

    [
      {1, 1},
      {3, 1},
      {5, 1},
      {7, 1},
      {1, 2}
    ]
    |> Stream.map(fn {right, down} ->
      map
      |> traverse(right, down)
      |> Stream.flat_map(& &1)
      |> Stream.filter(& &1 == "X")
      |> Enum.count
    end)
    |> Enum.reduce(1, & &1 * &2)
    |> IO.puts
  end

  defp traverse(map, right, down) do
    map
    |> Stream.with_index
    |> Enum.map(fn
      {row, index} when index > 0 and rem(index, down) == 0 ->
        x_offset = rem(div(index, down) * right, Enum.count(row))
        marker = if Enum.at(row, x_offset) == "#", do: "X", else: "O"
        row |> List.replace_at(x_offset, marker)
      {row, _index} -> row
    end)
  end
end

Script.main(System.argv)
